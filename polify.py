#!/home/qfwfq/Programs/environments/polify-env/bin/python


# Libraries
from menuslib import PoliMenu, PoliTools
from transform import PoliTransform
from scipy.ndimage import imread
from scipy.misc import imsave
from cv2 import VideoCapture, cvtColor, COLOR_BGR2RGB
import numpy as np
from screen import Screen



def main():
    # First, we gather an source file from the user. This is either a filepath
    #   to an image or video, or we'll be given the string 'webcam'
    source_filepath = get_source_filepath()
    filetype = get_filetype(source_filepath)

    # Deploy the tool panel for user control of parameters
    tools = PoliTools()
    transform = PoliTransform()

    if filetype == 'image':
        # Grab source image
        source_image = read_image(source_filepath)

    if filetype == 'video':
        if source_filepath == 'webcam':
            # Link to webcam and grab a test frame for sizing
            cap = VideoCapture(0)
            source_image = get_frame(cap, webcam_to_standard)

        else:
            # Load video to buffer and grab a test frame for sizing
            cap = VideoCapture(source_filepath)
            source_image = get_frame(cap)

    # Gather image size information
    size = (width, height) = (source_image.shape[0], source_image.shape[1])
    screen = Screen(size)

    while True:
        # Grab new frame if processing a video
        if filetype == 'video':
            if source_filepath == 'webcam':
                source_image = get_frame(cap, webcam_to_standard)
            else:
                source_image = get_frame(cap)

        # Get user control parameters
        parameters = tools.get_parameters()
        transform.update(parameters)

        # Apply transforms
        image = transform.apply(source_image)

        screen.display(image)

#        imsave("/home/qfwfq/Pictures/polify-output.jpg", image)



# Returns the next frame in the VideoCapture object's buffer.
def get_frame(cap, transform=None):
    rc = False
    while not rc:
        rc, source_image = cap.read()

    if transform:
        source_image = transform(source_image)

    return source_image


# Read image in from source and correctly transform format.
def read_image(source_filepath, format='pygame'):
    image = imread(source_filepath)

    # Pygame interprets numpy image arrays differently than matplotlib does.
    #   This means that for pygame to display an image array correctly, it
    #   must be read from source, then rotated 90 degrees.
    if format == 'pygame':
        return np.rot90(image)

    return image


# Takes a numpy array style image and maps from webcam to standard
def webcam_to_standard(source_image):
    image = np.rot90(source_image)
    image = cvtColor(image, COLOR_BGR2RGB)
    return image


# Deploys a pop-up window with text entry for the user to provide a filepath
def get_source_filepath():
    menu = PoliMenu()
    source = ''
    while not source:
        parameters = menu.get_parameters()
        source = parameters['source']
    return source


# Takes a filename which includes a format extension and returns a format from {'video', 'image'}
def get_filetype(filename):
    image_extensions = ['.jpg', '.jpeg', '.png']
    video_extensions = ['.avi', '.mp4']
    partial_match = lambda substrings, string: sum([substring in string for substring in substrings])
    filetype = 'video' if partial_match(video_extensions, filename) else \
               'image' if partial_match(image_extensions, filename) else 'video'    # Returns 'video' if 'webcam' is given
    return filetype



if __name__ == '__main__':
    main()
