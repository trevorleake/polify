import pygame

class Screen:
    def __init__(self, size):
        # Create screen object
        pygame.init()
        self.size = size
        self.screen = pygame.display.set_mode(size)

    def display(self, image):
        pygame.surfarray.blit_array(self.screen, image)
        pygame.display.update()
