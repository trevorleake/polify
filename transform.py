#!/home/qfwfq/Programs/environments/polify-env/bin/python

from cv2 import Canny
from random import shuffle
from itertools import product
from scipy.spatial import Delaunay
from PIL import Image, ImageDraw
from colormaps import map_grab
import numpy as np


class PoliTransform:
    def __init__(self, parameters={}):
        self.parameters = {
            'corners': True,
            'random': True,
            'num_vertices': 1000,
            'fill_polygons': True,
            'sectored': True,
            'edges': False,
            'sector_width': 100,
            'low_threshold': 100,
            'high_threshold': 200,
            'color_map': None
        }
        self.parameters.update(parameters)
        self.update(self.parameters)


    # Allows updating of transform, avoiding allocation for each change.
    def update(self, parameters):
        self.parameters.update(parameters)
        self.corners = self.parameters['corners']
        self.random = self.parameters['random']
        self.num_vertices = self.parameters['num_vertices']
        self.fill_polygons = self.parameters['fill_polygons']
        self.sectored = self.parameters['sectored']
        self.edges = self.parameters['edges']
        self.sector_width = self.parameters['sector_width']
        self.low_threshold = self.parameters['low_threshold']
        self.high_threshold = self.parameters['high_threshold']

        self.cmap = map_grab(self.parameters['color_map'])


    # Apply the "transformation" indicated by given parameters to source_image
    def apply(self, source_image):
        # Get size information
        width, height = (source_image.shape[0], source_image.shape[1])

        # Manipulations will generally add pixels to this set of Delaunay vertices.
        pixel_set = set()

        if self.random:
            # Create image where only edges show
            edged_image = Canny(source_image, self.low_threshold, self.high_threshold)

            # Gather all the edge pixels and take a random selection
            edge_pixels = self.get_nonzero_pixels(edged_image)
            pixel_set |= set(random_selection(edge_pixels, self.num_vertices))

        if self.corners:
            pixel_set |= set([(0, 0), (width-1, 0), (width-1, height-1), (0, height-1)])

        if self.sectored:
            sectors_wide = width / self.sector_width + 1
            sectors_high = height / self.sector_width + 1

            # Used to center the sector grid on the image
            x_offset = -((sectors_wide * self.sector_width) - width) / 2
            y_offset = -((sectors_high * self.sector_width) - height) / 2

            # Determine the 4-way meeting vertices' positions
            x_coords = [(i*self.sector_width) + x_offset for i in range(sectors_wide+1)]
            y_coords = [(j*self.sector_width) + y_offset for j in range(sectors_high+1)]

            # For any sector vertices which fall outside image bounds, place them on the image's edge
            in_bounds = lambda val, bound: 0 if val < 0 else bound-1 if val >= bound else val
            sector_vertices = [(in_bounds(x, width), in_bounds(y, height)) for (x, y) in product(x_coords, y_coords)]
            pixel_set |= set(sector_vertices)

        image = self.polify(source_image, pixel_set)
        return image



    def polify(self, source_image, pixel_set):
        size = (width, height) = (source_image.shape[0], source_image.shape[1])

        centroid = lambda x: ((x[0][1]+x[1][1]+x[2][1])/3, (x[0][0]+x[1][0]+x[2][0])/3)

        pixel_set = list(pixel_set)
        # Find a set of triangles using the Delaunay algorithm to tesselate the picture
        if len(pixel_set) >= 3:
            triangles = [(pixel_set[a], pixel_set[b], pixel_set[c]) for a, b, c in Delaunay(pixel_set).simplices]
        else:
            return source_image

        # Determine coloration for each triangle in the set by pinging the centroid pixel
        colors = {}
        for triangle in triangles:
            c = centroid(triangle)
            centroid_color = source_image[c[1]][c[0]]
            colors[triangle] = self.cmap(centroid_color) if self.cmap else centroid_color

        # Create blank image on which to draw our polygons
        polyified_image = Image.new("RGB", size)
        draw = ImageDraw.Draw(polyified_image)

        # Draw each polygon obeying given coloration commands
        for triangle in triangles:
            draw.polygon(
                triangle,
                fill=tuple(colors[triangle]) if self.fill_polygons else (0,0,0),
                outline=None if self.fill_polygons else tuple(colors[triangle])
            )
        return np.rot90(np.array(polyified_image))


    # Takes a grayscale image and returns the coordinates of all edge pixels
    def get_nonzero_pixels(self, edged_image):
        xs, ys = np.nonzero(edged_image)
        edge_pixels = zip(xs, ys)
        return edge_pixels


# Returns n randomly selected items from an iterable
def random_selection(iterable, n):
    shuffle(iterable)
    return iterable[:n]








def main():
    import numpy as np
    from scipy.ndimage import imread

    # Read image in from source and correctly transform format.
    def read_image(source_filepath, format='pygame'):
        image = imread(source_filepath)
        # Pygame interprets numpy image arrays differently than matplotlib does.
        #   This means that for pygame to display an image array correctly, it
        #   must be read from source, then rotated 90 degrees.
        if format == 'pygame':
            return np.rot90(image)
        return image


    transform = PoliTransform({
        'num_vertices': 2000,
    })

    source_image = read_image('/h1.jpg')

    transform.apply(source_image)


if __name__ == '__main__':
    main()
