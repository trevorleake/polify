#!/home/qfwfq/Programs/environments/polify-env/bin/python


from Tkinter import Scale, Checkbutton, HORIZONTAL, IntVar, Tk, Listbox, StringVar, Entry, Label, LEFT, END, Frame, OptionMenu, BOTH

MAX_VERTICES = 2000
MAX_SECTOR_WIDTH = 500


class PoliMenu(Frame):
    def __init__(self):
        default_color = '#%02x%02x%02x' % (210,210,210)

        # Create menu
        self.master = Tk()
        self.master.configure(bg=default_color)
        self.master.tk_setPalette(background=default_color, fg=default_color)

        # Control variable
        self.i = StringVar()

        # Box for easier positioning within master
        self.box = Listbox(self.master)
        self.box.pack()

        label = Label(self.box, text='Source filepath')
        label.pack(side=LEFT, padx=5)

        # Entry field
        i_entry = Entry(self.box, textvariable=self.i)
        i_entry.pack()
        i_entry.insert(END, 'webcam')

        self.parameters = {'source': ''}

        def submit(event):
            self.parameters['source'] = i_entry.get()
            self.master.destroy()

        self.master.bind('<Return>', submit)
#        self.master.bind('<Escape>'), self.master.destroy)

    def get_parameters(self):
        self.master.update_idletasks()
        self.master.update()
        return self.parameters



class PoliTools:
    def __init__(self):
        default_color = '#%02x%02x%02x' % (210,210,210)

        # Create menu
        self.master = Tk()
        self.master.configure(bg=default_color)
        self.master.tk_setPalette(background=default_color, fg=default_color)

        # Control variables
        self.v = IntVar()
        self.f = IntVar()
        self.s = IntVar()
        self.c = IntVar()
        self.r = IntVar()
        self.e = IntVar()
        self.sw = IntVar()
        self.cm = StringVar()

        # Box for easier layout manipulation
        self.box = Listbox(self.master)
        self.box.pack()

        # Insert each widget with its control variable
        v_slider = Scale(self.box, from_=1, to=MAX_VERTICES, orient=HORIZONTAL, label="vertices", variable=self.v, showvalue=0)
        v_slider.pack(anchor='w')
        v_slider.set(1000) # Default value is 1000

        sw_slider = Scale(self.box, from_=10, to=MAX_SECTOR_WIDTH, orient=HORIZONTAL, label="sector width", variable=self.sw, showvalue=0)
        sw_slider.pack(anchor='w')
        sw_slider.set(100) # Default value is 100

        f_button = Checkbutton(self.box, text="fill polygons", variable=self.f)
        f_button.pack(anchor='w')

        s_button = Checkbutton(self.box, text="sectored", variable=self.s)
        s_button.pack(anchor='w')

        c_button = Checkbutton(self.box, text="corners", variable=self.c)
        c_button.pack(anchor='w')

        r_button = Checkbutton(self.box, text="random", variable=self.r)
        r_button.pack(anchor='w')
        r_button.select() # Default value is True

        e_button = Checkbutton(self.box, text="edges", variable=self.e)
        e_button.pack(anchor='w')

        Label(self.box, text='\ncolor map').pack(anchor='w')
        cm_menu = OptionMenu(self.box, self.cm, 'none', 'black and white', 'washed out')
        cm_menu.pack(anchor='w', fill=BOTH)
        self.cm.set('none')


    def get_parameters(self):
        parameters = {
            "num_vertices": self.v.get(),
            "fill_polygons": self.f.get(),
            "corners": self.c.get(),
            "sectored": self.s.get(),
            "random": self.r.get(),
            "edges": self.e.get(),
            "sector_width": self.sw.get(),
            "color_map": self.cm.get()
        }
        self.master.update_idletasks()
        self.master.update()
        return parameters


def main():
    menu = PoliTools()
#    while True:
#        print menu.get_parameters()
    while 1:
        print menu.get_parameters()

if __name__ == '__main__':
    main()
