
black_and_white_cmap = lambda (r, g, b): (
    int((r**2 + g**2 + b**2) ** 0.5),
    int((r**2 + g**2 + b**2) ** 0.5),
    int((r**2 + g**2 + b**2) ** 0.5)
)

washed_out_cmap = lambda (r, g, b): (
    min((r+100), 255),
    min((g+100), 255),
    min((b+100), 255)
)

def rgb_to_hsv((r, g, b)):
    rp, gp, bp = [x/255.0 for x in (r, g, b)]
    cmax = max([rp, gp, bp])
    cmin = min([rp, gp, bp])
    delta = cmax-cmin

    if cmax == rp:
        h = 60 * (((gp-bp) / delta) % 6)
    if cmax == gp:
        h = 60 * (((bp-rp)/delta) + 2)
    if cmax == bp:
        h =  60 * (((rp-gp)/delta) + 4)

    h = int(h)
    s = 0 if cmax == 0 else delta/cmax
    v = cmax
    return (h, s, v)

def map_grab(map_name):
    if map_name == 'black and white':
        return black_and_white_cmap
    if map_name == 'washed out':
        return washed_out_cmap
    return None
