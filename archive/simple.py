#from itertools import permutations
from scipy.misc import imread
import cv2
from itertools import product
from itertools import combinations
from numpy.random import shuffle
from sklearn.cluster import KMeans
from PIL import Image
from time import sleep
import numpy as np
import sys


def dist(a, b):
    return np.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)

def area((a, b, c)):
	return max([dist(a, b), dist(b, c), dist(c, a)])	
#	s = dist(a, b) + dist(b, c) + dist(c, a) / 2
#	return np.sqrt(s*(s-dist(a,b))*(s-dist(b,c))*(s-dist(c,a)))

def inTriangle(p, (a, b, c)):
    def det(x, y, z): 
        return (x[0]-z[0]) * (y[1]-z[1]) - (y[0]-z[0]) * (x[1]-z[1])

    s = det(p, a, b) < 0
    t = det(p, b, c) < 0
    r = det(p, c, a) < 0
    return (s == t) and (t == r)

def linesIntersect(p1, p2, q1, q2):
	def cross(x, y):
		return x[0]*y[1] - x[1]*y[0]

	def sub(x, y):
		return (x[0]-y[0], x[1]-y[1])

	r = sub(p2, p1)
	s = sub(q2, q1)

	if cross(r, s) == 0 and cross(sub(q1, p1), r) == 0:	# Collinear and may be disjoint
		return False
	if cross(r, s) == 0 and cross(sub(q1, p1), r) != 0:	# Parallel and non-intersecting
		return False

	t = float(cross(sub(q1, p1), s)) / cross(r, s)
	u = float(cross(sub(q1, p1), r)) / cross(r, s)

	if cross(r, s) != 0 and u > 0 and u < 1 and t > 0 and t < 1:	# Intersecting
		return True
	return False	# Non-parallel and not intersecting

def intersects((a,b,c), (d,e,f)):
    return linesIntersect(a,b,d,e) or \
    linesIntersect(a,b,e,f) or \
    linesIntersect(a,b,f,d) or \
    linesIntersect(b,c,d,e) or \
    linesIntersect(b,c,e,f) or \
    linesIntersect(b,c,f,d) or \
    linesIntersect(c,a,d,e) or \
    linesIntersect(c,a,e,f) or \
    linesIntersect(c,a,f,d)

def mean_color(pixels):
	mean_color = [0, 0, 0]
	for pixel in pixels:
		mean_color[0] += pixel[0]
		mean_color[1] += pixel[1]
		mean_color[2] += pixel[2]
	mean_color[0] = mean_color[0]/len(pixels) if len(pixels) else 0
	mean_color[1] = mean_color[1]/len(pixels) if len(pixels) else 0
	mean_color[2] = mean_color[2]/len(pixels) if len(pixels) else 0
	return tuple(mean_color)

def color_dist(a, b):
	return max((a[0]-b[0], a[1]-b[1], a[2]-b[2]))

def color_cohesion(pixels):
	max_dist = max([max([dist(pa, pb) for pa in pixels]) for pb in pixels])
	return 1/max_dist if max_dist else 0

def neighborhood(coord):
	x, y = coord
	neighbors = [
		(x-1, y-1),
		(x-1, y+0),
		(x-1, y+1),
		(x+0, y-1),
		(x+0, y+0),
		(x+0, y+1),
		(x+1, y-1),
		(x+1, y+0),
		(x+1, y+1)
	]
	return neighbors



def main():
	im = Image.open('/home/qfwfq/Pictures/butterfly.jpg')
	pix = im.load()
	width, height = im.size
	pixels = [[(255, 255, 255) for i in range(width)] for j in range(height)]

	coords = list(product(range(1, width-1), range(1, height-1)))

	shuffle(coords)
	coords = coords[:1000]

	print "Vertices created"
	pixel_groups = []
	for coord in coords:
		pixel_groups.append([pix[x, y] for x, y in neighborhood(coord)])
	coords = sorted(coords, key=lambda x: color_cohesion(pixel_groups[coords.index(coord)]))
	print "Vertices sorted"

	vertices = coords[:600]
	'''
	for coord in coords:
		for x, y in neighborhood(coord):
			pix[x, y] = (255, 0, 0)
	im.save('out.jpg')
	'''


	model = KMeans(n_clusters=10)
	data = [list(pix[x,y]) + [x, y] for x, y in vertices]	
	labels = model.fit_predict(data)
	clusters = dict([(label, []) for label in labels])
	for (r, g, b, x, y), label in zip(data, labels):
		clusters[label].append((x, y))
	print "Vertices clustered"	

	'''
	for n, label in enumerate(clusters):
		for r, g, b, i, j in clusters[label]:
			point = (i, j)
			for x, y in neighborhood(point):
				pix[x, y] = ((n*14)%255, (n*40)%255, (n*150)%255)
	im.save('out.jpg')
	'''

	#frames = [imread(path) for path in frame_paths]
	#edged = cv2.Canny(10, 10, 100, 200)

	n = 0
	for vertices in clusters.values():
		triangles = set(combinations(vertices, 3))
		triangles = sorted(triangles, key=lambda x: area(x))
		print len(triangles)

		for vertex in vertices:
			triangles = set(triangles) - set([x for x in triangles if inTriangle(vertex, x) and vertex not in x]) # bad triangles
		triangles = list(triangles)

		flat = [(255, 255, 255)]
		while triangles:
			current_triangle = triangles.pop(0)

			component_pixels = [point for point in product(range(width), range(height)) if inTriangle(point, current_triangle)]
			color = mean_color([pix[x, y] for x, y in component_pixels])

			for x, y in component_pixels:
		#		pixels[y][x] = color
				pix[x, y] = color

			triangles = [triangle for triangle in triangles if not intersects(current_triangle, triangle)]

		 #   	flat = []
	    	#	for x in pixels:
		#		flat += x

		# 	im = Image.new("RGB", (width, height))
		 #	im.putdata(flat)
		 #	im.show()
		    	n += 1
		print clusters.values().index(vertices)
		

	im.save('out.jpg')
	exit()

main()
