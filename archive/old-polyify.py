# TODO
#   Use binary erosions to thin edge lines to make distribution of vertices more uniform
#   Make rot90 and width height swaps solved
#   Attempt background createBackgroundSubtractorMOG2 use
#   add sectored quotas

import pygame
import cv2
import numpy as np
from random import shuffle
from scipy.spatial import Delaunay
from scipy.ndimage import imread
from scipy.misc import imsave
from PIL import Image
from PIL import ImageDraw
from itertools import product
from pprint import pprint as pp
import sys
import colormaps as cmps
from Tkinter import Tk, IntVar, Scale, HORIZONTAL, Checkbutton
from scipy import ndimage

def edge_detect(image, low_threshold=100, high_threshold=200):
	return cv2.Canny(image, low_threshold, high_threshold)


def display(image, screen):
    frame = pygame.surfarray.make_surface(image)
    rect = frame.get_rect()
    screen.blit(frame, rect)
    pygame.display.update()


def next_frame(source):
    _, frame = source.read()
    frame = np.rot90(frame)
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    return frame



# Load all Haar cascade data
face_cascade = cv2.CascadeClassifier('./resources/front-face.xml')
profile_face_cascade = cv2.CascadeClassifier('./resources/profile-face.xml')
eye_cascade = cv2.CascadeClassifier('./resources/eye.xml')
def detect_faces(image):
    global face_cascade, profile_face_cascade, eye_cascade

    image = np.rot90(image, 3)
    regions = []
    regions += [list(face) for face in face_cascade.detectMultiScale(image, 1.3, 5)]

#   Skip profile face detection
#    regions += [list(face) for face in profile_face_cascade.detectMultiScale(image, 1.3, 5)]

#    Skip eye detection
#    for (x, y, w, h) in regions:
#        face = image[x:x+w, y:y+h]
#        regions += [[eye[0]+x, eye[1]+y, eye[2], eye[3]] for eye in eye_cascade.detectMultiScale(face, 1.3, 5)]
    return [((width-x-w), y, w, h) for (x, y, w, h) in regions]



def nonzero_pixels(image):
    xs, ys = np.nonzero(image)
    return zip(xs, ys)


def random_selection(pixel_coords, num_vertices):
    shuffle(pixel_coords)
    return pixel_coords[:num_vertices]


def select_from(image, num_vertices=1000, regions=[]):
    if not regions:
        regions = [(0, 0, width, height)]

    # Find image edges
    edged_image = edge_detect(image)

    # Grab image sections indicated by each given region
    image_regions = [((x, y), edged_image[x:x+w, y:y+h]) for (x, y, w, h) in regions]

    # Create a set containing random selections of edge pixels from each region
    pixel_set = []
    for (x_offset, y_offset), image_region in image_regions:
        # Gather coordinates of the edge pixels
        edge_px = [(x+x_offset, y+y_offset) for (x, y) in nonzero_pixels(image_region)]
        pixel_set += random_selection(edge_px, num_vertices)
    return pixel_set


def polyify(image, pixel_set, fill, cmap=None):
    centroid = lambda x: ((x[0][1]+x[1][1]+x[2][1])/3, (x[0][0]+x[1][0]+x[2][0])/3)
    global size, width, height

    # Find a set of triangles using the Delaunay algorithm to tesselate the picture
    if len(pixel_set) >= 3:
        triangles = [(pixel_set[a], pixel_set[b], pixel_set[c]) for a, b, c in Delaunay(pixel_set).simplices]
    else:
        return image

    # Determine coloration for each triangle in the set by pinging the centroid pixel
    colors = {}
    for triangle in triangles:
        c = centroid(triangle)
        centroid_color = image[c[1]][c[0]]
        colors[triangle] = cmap(centroid_color) if cmap else centroid_color

#        (r, g, b) = colors[triangle]
#        scalar=230
#        colors[triangle] = ((r+scalar)%255, (g+scalar)%255, (b+scalar)%255)

    # Create blank image on which to draw our polygons
    polyified_image = Image.new("RGB", size)
    draw = ImageDraw.Draw(polyified_image)

    # Draw each polygon obeying given coloration commands
    for triangle in triangles:
        draw.polygon(
            triangle,
            fill=tuple(colors[triangle]) if fill else (0,0,0),
            outline=None if fill else tuple(colors[triangle])
        )
    return np.rot90(np.array(polyified_image))


def get_sector_vertices(sector_side_length):
    global height, width
    sectors_wide = width / sector_side_length + 1
    sectors_high = height / sector_side_length + 1

    # Used to center the sector grid on the image
    x_offset = -((sectors_wide * sector_side_length) - width) / 2
    y_offset = -((sectors_high * sector_side_length) - height) / 2

    # Determine the 4-way meeting vertices' positions
    x_coords = [(i*sector_side_length) + x_offset for i in range(sectors_wide+1)]
    y_coords = [(j*sector_side_length) + y_offset for j in range(sectors_high+1)]

    # List all 4-way meeting vertices as valid (in bounds) objects
    in_bounds = lambda val, bound: 0 if val < 0 else bound-1 if val >= bound else val
    sector_vertices = [(in_bounds(x, width), in_bounds(y, height)) for (x, y) in product(x_coords, y_coords)]
    return sector_vertices



filepath = sys.argv[sys.argv.index('-f') + 1] if '-f' in sys.argv else 0
filetype = 'video' if filepath == 0 or '.mp4' in filepath or '.avi' in filepath else 'image'


def main():
    #####################
    ### PREPROCESSING ###
    #####################

    # Initialize video capture object and pygame library
    pygame.init()

    if filetype == "image":
        if filepath:
            input_image = imread(filepath)
        else:
            cap = cv2.VideoCapture(0)
            input_image = next_frame(cap)

    if filetype == "video":
        if filepath:
            cap = cv2.VideoCapture(filepath)
        else:
            cap = cv2.VideoCapture(0)
        # Test image size to get sizing ratio
        input_image = next_frame(cap)


    # Gather width and height info on image
    global size, width, height
    size = (width, height) = (input_image.shape[0], input_image.shape[1])
    size = (1000,1000)

    # Create screen object
    screen = pygame.display.set_mode(size)

    faces = []
    regions = []    # Image regions to polyify
    frame_num = 0   # Current frame number
    corners = [(0, 0), (width-1, 0), (width-1, height-1), (0, height-1)]

    #################
    ### MAIN LOOP ###
    #################
    while(1):



        if filetype == "video":
            # Read image in, rotate, and color-correct
            input_image = next_frame(cap)

        frame_num += 1
        pixel_set = []
        regions = []
        image = None

        # Process image as indicated. Each case adds to a set of Delaunay vertices
        if parameters['edges']:
            # Find image edges
            image = edge_detect(input_image)
            image = np.flip(image, 0)
            image = ndimage.binary_erosion(image).astype(image.dtype)
            image = np.asarray(image)

        if parameters['random']:
            # Add full image as valid region
            if (0, 0, width, height) not in regions:
                regions += [(0, 0, width, height)]

        if parameters['detect_faces']:
            # Periodically gather regions encapsulating detected faces
            if frame_num % 3 == 0:
                faces = detect_faces(input_image)
            regions += faces
            print faces

        if regions:
            # Select edge pixels randomly from given regions
            pixel_set += select_from(input_image, parameters['num_vertices'], regions)

        if parameters['corners']:
            # Add the four corners as vertices
            pixel_set += corners

        if parameters['sectored']:
            # Add regularly spaces grid vertices
            pixel_set += get_sector_vertices(parameters['sector_width'])


        if pixel_set:
#            cmap = cmps.black_and_white_cmap
            cmap = None

            # Tesselate the image using the given pixels as Delaunay vertices
            image = polyify(input_image, pixel_set, parameters['fill_polygons'], cmap)

        if image is not None:

            if filetype == "image":
                image = np.rot90(image, 3)
                imsave("output.jpg", image)

            # Display image to window in real-time
            display(image, screen)



if __name__ == "__main__":
    main()
