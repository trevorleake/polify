from itertools import combinations
from numpy.random import shuffle
from PIL import Image
import numpy as np


def meanColor(colors):
	mean = sum([np.asarray(color) for color in colors])
	try:
		return mean/len(colors)
	except:
		return [0,0,0]

def inTriangle(p, (a, b, c)):
	def det(x, y, z):
		return (x[0]-z[0]) * (y[1]-z[1]) - (y[0]-z[0]) * (x[1]-z[1])

	s = det(p, a, b) < 0
	t = det(p, b, c) < 0
	r = det(p, c, a) < 0
	return (s == t) and (t == r)



def neighborhood((x, y)):
	points = []
	for i in range(-1, 2):
		for j in range(-1, 2):
			points.append((i+x, j+y))
	return points

def dist(a, b):
	return np.sqrt((a[0]-b[0])**2 + (a[1]-b[1])**2)

def area((a, b, c)):
	s = dist(a, b) + dist(b, c) + dist(c, a) / 2
	return np.sqrt(s*(s-dist(a,b))*(s-dist(b,c))*(s-dist(c,a)))

def linesIntersect(p1, p2, q1, q2):
	def cross(x, y):
		return x[0]*y[1] - x[1]*y[0]

	def sub(x, y):
		return (x[0]-y[0], x[1]-y[1])

	r = sub(p2, p1)
	s = sub(q2, q1)

	if cross(r, s) == 0 and cross(sub(q1, p1), r) == 0:	# Collinear and may be disjoint
		return False
	if cross(r, s) == 0 and cross(sub(q1, p1), r) != 0:	# Parallel and non-intersecting
		return False

	t = float(cross(sub(q1, p1), s)) / cross(r, s)
	u = float(cross(sub(q1, p1), r)) / cross(r, s)

	if cross(r, s) != 0 and u > 0 and u < 1 and t > 0 and t < 1:	# Intersecting
		return True
	return False	# Non-parallel and not intersecting

def intersects((a,b,c), (d,e,f)):
	return linesIntersect(a,b,d,e) or \
	linesIntersect(a,b,e,f) or \
	linesIntersect(a,b,f,d) or \
	linesIntersect(b,c,d,e) or \
	linesIntersect(b,c,e,f) or \
	linesIntersect(b,c,f,d) or \
	linesIntersect(c,a,d,e) or \
	linesIntersect(c,a,e,f) or \
	linesIntersect(c,a,f,d)
