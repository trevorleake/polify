
import cv2
from PIL import Image
import pygame
from itertools import product
from itertools import combinations
from numpy.random import shuffle
from sklearn.cluster import KMeans
from PIL import Image
import numpy as np
import sys
from scipy.spatial import Delaunay
from helpers import *
import cv2
from PIL import ImageDraw
from scipy.misc import imsave

args = sys.argv

num_vertices = int(args[args.index('-v')+1]) if '-v' in args else \
	       int(args[args.index('--vertices')+1]) if '--vertices' in args else \
	       1000

low_threshold = int(args[args.index('-l')+1]) if '-l' in args else \
	       	int(args[args.index('--low')+1]) if '--low' in args else \
	       	100

high_threshold = int(args[args.index('-h')+1]) if '-h' in args else \
	         int(args[args.index('--high')+1]) if '--high' in args else \
	         200

mode = args[args.index('-m')+1] if '-m' in args else \
       args[args.index('--mode')+1] if '--mode' in args else \
       'sectored'

fill = '-f' in args or '--fill' in args 
detect = '-d' in args or '--detect' in args 

if mode == 'detect-only': detect = True



centroid = lambda x: ((x[0][1]+x[1][1]+x[2][1])/3, (x[0][0]+x[1][0]+x[2][0])/3)
def stuff(image, regions=[]):
	edged_image = cv2.Canny(image, low_threshold, high_threshold)

	if mode == 'edges':
		return Image.fromarray(edged_image)

	width = len(edged_image)
	height = len(edged_image[0])


	if mode == 'random':
		temp = np.nonzero(edged_image)
		edge_px = [(x, y) for x, y in zip(temp[0], temp[1])]
		shuffle(edge_px)
		edge_px = edge_px[:num_vertices]

	if mode == 'sectored':
		sectors_in_row = 5
		num_sectors = 25			# Must be square number
		sectors = dict(((i, j), None) for (i, j) in product(range(sectors_in_row), range(sectors_in_row)))
		sector_width = width/sectors_in_row	# Distance in px
		sector_height = height/sectors_in_row	# Distance in px

		quota = num_vertices/num_sectors

		for (i, j) in product(range(sectors_in_row), range(sectors_in_row)):
				sectors[(i, j)] = edged_image[i*sector_width:(i+1)*sector_width, j*sector_height:(j+1)*sector_height]

		edge_px = []
		for (i, j), sector in sectors.items():
			a, b = np.nonzero(sector)
			edges = zip([x+i*sector_width for x in a], [(y+j*sector_height) for y in b])

			shuffle(edges)
			edge_px += edges[:quota if quota <= len(edges) else len(edges)]

	if mode == 'detect-only':
		edge_px = []
		for (x, y, w, h) in regions:
			x -= 15
			y -= 15
			w += 30
			h += 30
			sector = edged_image[x:(x+w), y:(y+h)]
			a, b = np.nonzero(sector)
			edges = zip([x+z for z in a], [(y+z) for z in b])
			shuffle(edges)
			edge_px += edges[:int(len(edges)/2)]

	else:
		if regions:
			for (x, y, w, h) in regions:
				sector = edged_image[x:(x+w), y:(y+h)]
				a, b = np.nonzero(sector)
				edges = zip(a, b)
				shuffle(edges)
				edge_px += edges[:len(edges)/3]

	added = [(50*i, 50*j) for i, j in product(range(width/50), range(height/50))]
	added += [(0, 0), (width-1, 0), (0, height-1), (width-1, height-1)]
	edge_px += added


	triangles = [(edge_px[a], edge_px[b], edge_px[c]) for a, b, c in Delaunay(edge_px).simplices]

	colors = {}
	for triangle in triangles:
		c = centroid(triangle)
		colors[triangle] = image[c[1]][c[0]]

	im = Image.new("RGB", (height*2, width*2))
	draw = ImageDraw.Draw(im)
	for triangle in triangles:
		a, b, c = triangle
		a = (a[1]*2, a[0]*2)
		b = (b[1]*2, b[0]*2)
		c = (c[1]*2, c[0]*2)

		if fill:
			draw.polygon((a, b, c), fill=tuple(colors[triangle]))
		else:
			draw.polygon((a, b, c), fill=(0,0,0), outline=tuple(colors[triangle]))
	return im



def main():
	pygame.init()
	size = width, height = 1200, 800
	screen = pygame.display.set_mode(size)

	cap = cv2.VideoCapture(0)

	face_cascade = cv2.CascadeClassifier('/home/qfwfq/Desktop/front-face.xml')
	profile_face_cascade = cv2.CascadeClassifier('/home/qfwfq/Desktop/profile-face.xml')
	eye_cascade = cv2.CascadeClassifier('/home/qfwfq/Desktop/eye.xml')

	n = 0
	while(1):
		_, im = cap.read()
		im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

		regions = []
		if detect:
			regions += [list(face) for face in face_cascade.detectMultiScale(im, 1.3, 5)]
			regions += [list(face) for face in profile_face_cascade.detectMultiScale(im, 1.3, 5)]
			for (x, y, w, h) in regions:
				face = im[x:x+w, y:y+h]
#				regions += [[eye[0]+x, eye[1]+y, eye[2], eye[3]] for eye in eye_cascade.detectMultiScale(face, 1.3, 5)]

			for (x, y, w, h) in regions:
				cv2.rectangle(im, (x,y), (x+w,y+h), (0,255,0), 2)
			n = 0

		im = np.rot90(im)
		im = stuff(im, regions)
		im = np.array(im.convert("RGB"))
		
		imsave("out.jpg", im)
		frame = pygame.surfarray.make_surface(im)
		rect = frame.get_rect()
		screen.blit(frame, rect)

		pygame.display.flip()
		n += 1


main()
