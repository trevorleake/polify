from itertools import product
from itertools import combinations
from numpy.random import shuffle
from sklearn.cluster import KMeans
from PIL import Image
import numpy as np
import sys
from scipy.spatial import Delaunay
from helpers import *
import cv2
from PIL import ImageDraw


args = sys.argv

filename = args[args.index('-i')+1] if '-i' in args else \
	   args[args.index('--input')+1] if '--input' in args else \
	   raw_input('Please enter a filename: ')

num_vertices = int(args[args.index('-v')+1]) if '-v' in args else \
	       int(args[args.index('--vertices')+1]) if '--vertices' in args else \
	       1000

low_threshold = int(args[args.index('-l')+1]) if '-l' in args else \
	       	int(args[args.index('--low')+1]) if '--low' in args else \
	       	100

high_threshold = int(args[args.index('-h')+1]) if '-h' in args else \
	         int(args[args.index('--high')+1]) if '--high' in args else \
	         200

mode = args[args.index('-m')+1] if '-m' in args else \
       args[args.index('--mode')+1] if '--mode' in args else \
       'sectored'

fill = args[args.index('-f')+1] if '-f' in args else \
       args[args.index('--fill')+1] if '--fill' in args else \
       'on'

output = args[args.index('-o')+1] if '-o' in args else \
      	 args[args.index('--output')+1] if '--output' in args else \
       	 None





image = cv2.imread(filename)
edged_image = cv2.Canny(image, low_threshold, high_threshold)

if mode == 'edges':
	Image.fromarray(edged_image).show()
	exit()

width = len(edged_image)
height = len(edged_image[0])

edge_px = []
for i in range(width):
	for j in range(height):
		if edged_image[i][j] > 0:
			edge_px.append((i, j))

if mode == 'random':
	shuffle(edge_px)
	edge_px = edge_px[:num_vertices]

if mode == 'sectored':
	shuffle(edge_px)
	goodies = []

	sectors_in_row = 10
	sector_width = width/sectors_in_row
	sector_height = height/sectors_in_row

	quota = num_vertices / sectors_in_row**2

	n = 0
	for i, j in product(range(sectors_in_row), range(sectors_in_row)):
		sector = []
		first_n = n
		n += 1
		while len(sector) < quota and first_n != n:
			p = edge_px[n]
			in_x = lambda p: p[0] >= i*sector_width and p[0] < (i+1)*sector_width
			in_y = lambda p: p[1] >= j*sector_height and p[1] < (j+1)*sector_height
			if in_x(p) and in_y(p):
				sector.append(p)

			if n >= len(edge_px)-1:
				n = 0 
			else:
				n += 1
		goodies += sector
	edge_px = goodies



added = [(50*i, 50*j) for i, j in product(range(width/50), range(height/50))]
added += [(0, 0), (width-1, 0), (0, height-1), (width-1, height-1)]
edge_px += added


triangles = [(edge_px[a], edge_px[b], edge_px[c]) for a, b, c in Delaunay(edge_px).simplices]

im = Image.open(filename)
pix = im.load()

centroid = lambda x: ((x[0][1]+x[1][1]+x[2][1])/3, (x[0][0]+x[1][0]+x[2][0])/3)
colors = dict((triangle, pix[centroid(triangle)]) for triangle in triangles)

im = Image.new("RGB", (height, width))
draw = ImageDraw.Draw(im)
for triangle in triangles:
	a,b,c = triangle
	a = (a[1], a[0])
	b = (b[1], b[0])
	c = (c[1], c[0])

	if fill == 'on':
		draw.polygon((a, b, c), fill=tuple(colors[triangle]))
	else:
		draw.polygon((a, b, c), fill=(0,0,0), outline=tuple(colors[triangle]))
#(255,255,255))

if output:
	im.save(output)
else:
	im.show()
exit()




