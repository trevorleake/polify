# TODO
#   Better flow for main
#   Tkinter menu and slider system for inputs
#   Make rot90 and width height swaps solved
#   Attempt background createBackgroundSubtractorMOG2 use


import pygame
import cv2
import numpy as np
from random import shuffle
from scipy.spatial import Delaunay
from PIL import Image
from PIL import ImageDraw
from itertools import product
from pprint import pprint as pp
import sys


def edge_detect(image, low_threshold=100, high_threshold=200):
	return cv2.Canny(image, low_threshold, high_threshold)


def mask(a, b):
    imA = Image.fromarray(a)
    imB = Image.fromarray(b)
#    imB = Image.fromarray(np.rot90(b))
    c = Image.new('1', b.shape[::-1], 0)
    print imA.size, imB.size, c.size
    return np.array(Image.composite(imA, imB, c))


def detect_faces(im):
    global face_cascade
    global profile_face_cascade
    global eye_cascade

    regions = []
    regions += [list(face) for face in face_cascade.detectMultiScale(im, 1.3, 5)]
    regions += [list(face) for face in profile_face_cascade.detectMultiScale(im, 1.3, 5)]

    for (x, y, w, h) in regions:
        face = im[x:x+w, y:y+h]
        regions += [[eye[0]+x, eye[1]+y, eye[2], eye[3]] for eye in eye_cascade.detectMultiScale(face, 1.3, 5)]

#    for (x, y, w, h) in regions:
#        im = cv2.rectangle(im, (x,y), (x+w,y+h), (0, 255, 0), 2)
    return regions


def nonzero_pixels(image):
    xs, ys = np.nonzero(image)
    return zip(xs, ys)


def polyify(image, pixel_set, fill):
    centroid = lambda x: ((x[0][1]+x[1][1]+x[2][1])/3, (x[0][0]+x[1][0]+x[2][0])/3)

    size = width, height = (image.shape[0], image.shape[1])

    # Find a set of triangles using the Delaunay algorithm to tesselate the picture
    if len(pixel_set) >= 3:
        triangles = [(pixel_set[a], pixel_set[b], pixel_set[c]) for a, b, c in Delaunay(pixel_set).simplices]
    else:
        return image
        return np.array(Image.new("RGB", size))

    # Determine coloration for each triangle in the set by pinging the centroid pixel
    colors = {}
    for triangle in triangles:
        c = centroid(triangle)
        colors[triangle] = image[c[1]][c[0]]

    # Create blank image on which to draw our polygons
    polyified_image = Image.new("RGB", (width, height))
    draw = ImageDraw.Draw(polyified_image)

    # Draw each polygon obeying given coloration commands
    for triangle in triangles:
        draw.polygon(
            triangle,
            fill=tuple(colors[triangle]) if fill else (0,0,0),
            outline=None if fill else tuple(colors[triangle])
        )
    return np.rot90(np.rot90(np.rot90(np.array(polyified_image))))




fill = True
mode = "sectored"
num_vertices = 200
sector_side_length = 150


# Load all Haar cascade data
face_cascade = cv2.CascadeClassifier('/home/qfwfq/Desktop/front-face.xml')
profile_face_cascade = cv2.CascadeClassifier('/home/qfwfq/Desktop/profile-face.xml')
eye_cascade = cv2.CascadeClassifier('/home/qfwfq/Desktop/eye.xml')

def main():
    pygame.init()
    cap = cv2.VideoCapture(0)


#    cap = cv2.VideoCapture('/home/qfwfq/Downloads/hotline.mp4')
#    for i in range(int(sys.argv[-1])):
#        cap.read()
#    bg = cv2.createBackgroundSubtractorMOG2()


    # Test image size to get sizing ratio
    _, test_image = cap.read()

    # Create screen object
    size = width, height = (test_image.shape[1], test_image.shape[0])
    screen = pygame.display.set_mode(size)

    frame_num = 0
    regions = []

    while(1):
        # Read image in, rotate, and color-correct
        _, im = cap.read()
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)

        frame_num += 1  # To track progress. Only used in debugging

        # Process image as indicated. Each case concludes with a numpy image array constructed
        if mode == "edges":
            # Find image edges
            image = edge_detect(im)


        elif mode == "random":
            pixel_set = []  # Will hold the vertex pixels for polyfication

            # Find image edges
            edged_image = edge_detect(im)

            # Gather coordinates of the edge pixels
            edge_px = nonzero_pixels(edged_image)

            # Add a random selection of edge pixels to the vertex set and solve
            shuffle(edge_px)
            pixel_set += edge_px[:num_vertices]

            image = polyify(im, pixel_set, fill=fill)


        elif mode == "sectored":
            pixel_set = []  # Will hold the vertex pixels for polyfication

            sectors_wide = width / sector_side_length + 1
            sectors_high = height / sector_side_length + 1
            num_sectors = sectors_wide * sectors_high

            # Used to center the sector grid on the image
            x_offset = -((sectors_wide * sector_side_length) - width) / 2
            y_offset = -((sectors_high * sector_side_length) - height) / 2

            # Number of vertices to be placed in each sector for uniform distribution
            quota = num_vertices / num_sectors

            # Determine the 4-way meeting vertices' positions
            x_coords = [(i*sector_side_length) + y_offset for i in range(sectors_high+1)]
            y_coords = [(j*sector_side_length) + x_offset for j in range(sectors_wide+1)]
            sector_vertices = list(product(x_coords, y_coords))

            # Find image edges
            edged_im = edge_detect(im)

            # Partition the edge pixels into sector sets and round to legal values
            in_bounds = lambda val, bound: 0 if val < 0 else bound-1 if val >= bound else val
            sector_boundaries = [(
                in_bounds(x, width),
                in_bounds(y, height),
                in_bounds(x + sector_side_length, height),
                in_bounds(y + sector_side_length, width),
                ) for (x, y) in sector_vertices
            ]
            sectors = dict(((xl, yt), edged_im[xl:xr, yt:yb]) for (xl, yt, xr, yb) in sector_boundaries)

            # Grab a uniform number of pixels from each sector
            for (x, y), sector in sectors.items():
                # Find coordinates of all edge pixels
                xs, ys = np.nonzero(sector)
                edge_px = zip([x + a for a in xs], [y + b for b in ys])

                # Shuffle and randomly select up to quota many pixels
                shuffle(edge_px)
                pixel_set += edge_px[:min(quota, len(edge_px))]
            pixel_set += [(in_bounds(x, height), in_bounds(y, width)) for (x, y) in sector_vertices]

            # Add image corners
            pixel_set += [(0,0), (0,width-1), (height-1,width-1), (height-1,0)]

            image = polyify(im, pixel_set, fill)


        elif mode == "detect":
            if frame_num % 10 == 0:
                regions = detect_faces(im) or regions

            if regions:
                pixel_set = []
                edged_image = edge_detect(im)
                image_regions = [((x, y), edged_image[x-50:x+w+50, y-50:y+h+50]) for (x, y, w, h) in regions]
                edge_px = []
                for (x_offset, y_offset), image_region in image_regions:
                    edge_px = [(y_offset+y, x_offset+x) for (x, y) in nonzero_pixels(image_region)]
                    shuffle(edge_px)
                    pixel_set += edge_px[:num_vertices]

                image = polyify(im, pixel_set, fill)
            else:
                image = im


        image = np.rot90(image)

        # Display image to window in real-time
        frame = pygame.surfarray.make_surface(image)
        rect = frame.get_rect()
        screen.blit(frame, rect)
        pygame.display.flip()



if __name__ == "__main__":
    main()
